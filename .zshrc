# Tab highlight
### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit's installer chunk

# zinit ice pick"async.zsh" src"pure.zsh"
#zinit light sindresorhus/pure # 1 theme
#zinit light denysdovhan/spaceship-prompt # 2 theme
#zinit ice compile'(pure|async).zsh' pick'async.zsh' src'pure.zsh'
#zinit light chabou/pure-now
zinit ice depth=1; zinit light romkatv/powerlevel10k

#zinit load mafredri/zsh-async
#zinit ice src"shell/key-bindings.zsh"
zinit ice pick"shell/key-bindings.zsh"; zinit light junegunn/fzf
#zinit snippet "/usr/share/doc/fzf/examples/key-bindings.zsh"  
zinit load zsh-users/zsh-completions
zinit load zsh-users/zsh-autosuggestions
#zinit load zsh-users/zsh-syntax-highlighting
zinit load zdharma-continuum/fast-syntax-highlighting

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=1000

setopt SHARE_HISTORY
setopt HIST_FIND_NO_DUPS
setopt appendhistory

# === BIND KEY
bindkey "\e[1;5D" backward-word
bindkey "\e[1;5C" forward-word

# ctrl-bs and ctrl-del
bindkey "\e[3;5~" kill-word
bindkey "\C-_"    backward-kill-word

# del, home and end
bindkey "\e[3~" delete-char
bindkey "\e[H"  beginning-of-line
bindkey "\e[F"  end-of-line

# alt-bs
bindkey "\e\d"  undo

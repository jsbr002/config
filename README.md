zinit
===

```
sudo apt install zsh fzf curl git
sh -c "$(curl -fsSL https://git.io/zinit-install)"
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/JetBrainsMono.zip; mkdir ~/.local/share/fonts; unzip ./JetBrainsMono.zip -d ~/.local/share/fonts/; rm ~/JetBrainsMono.zip
curl https://gitlab.com/jsbr002/config/-/raw/master/.zshrc -o ~/.zshrc
mkdir -p ~/.local/share/xfce4/terminal/colorschemes
curl https://gitlab.com/jsbr002/config/-/raw/master/snazzy.theme -o ~/.local/share/xfce4/terminal/colorschemes/snazzy.theme
chsh -s $(which zsh)
zsh
```


NB: you should manualy set font to JetBrainsMono ans chose snazzy color in terminal setting

see https://github.com/romkatv/powerlevel10k

zsh4humans
====
use: https://github.com/romkatv/zsh4humans


zcomet
====
```
sudo apt install zsh fzf curl git
git clone https://github.com/agkozak/zcomet.git ~/.zcomet --depth 1
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip; mkdir ~/.local/share/fonts; unzip ~/JetBrainsMono.zip -d ~/.local/share/fonts/; rm ~/JetBrainsMono.zip
curl https://gitlab.com/jsbr002/config/-/raw/master/.zshr-comet -o ~/.zshrc
curl https://gitlab.com/jsbr002/config/-/raw/master/snazzy.theme -o ~/.local/share/xfce4/terminal/colorschemes/snazzy.theme
chsh -s $(which zsh)
```
